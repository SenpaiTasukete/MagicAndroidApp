package ml.neoforex.myapplication;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextClock;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private TextView textView;
    private TextView textView1;

    private int poisonP1 = 0;
    private int poisonP2 = 0;
    private int lifeP1 = 0;
    private int lifeP2 = 0;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_main, container, false);

        if(savedInstanceState != null)
            loadData(savedInstanceState);
        else
            reset();

        textView = inflate.findViewById(R.id.textView);
        textView1 = inflate.findViewById(R.id.textView2);
        refreshText();


        Button b1More = inflate.findViewById(R.id.bt1PMore);
        b1More.setOnClickListener(view -> doAction(view.getId()));

        Button b2Less = inflate.findViewById(R.id.bt2Less);
        b2Less.setOnClickListener(view -> doAction(view.getId()));

        Button b3More = inflate.findViewById(R.id.bt3More);
        b3More.setOnClickListener(view -> doAction(view.getId()));

        Button b4Less = inflate.findViewById(R.id.bt4Less);
        b4Less.setOnClickListener(view -> doAction(view.getId()));

        ImageButton p1lifemore = inflate.findViewById(R.id.pf1lifemore);
        p1lifemore.setOnClickListener(view -> doAction(view.getId()));

        ImageButton p2lifemore = inflate.findViewById(R.id.p2lifemore);
        p2lifemore.setOnClickListener(view -> doAction(view.getId()));

        ImageButton p1lifeless = inflate.findViewById(R.id.pf1lifeless);
        p1lifeless.setOnClickListener(view -> doAction(view.getId()));

        ImageButton p2lifeless = inflate.findViewById(R.id.pf2lifeless);
        p2lifeless.setOnClickListener(view -> doAction(view.getId()));

        Button p1ToP2 = inflate.findViewById(R.id.p1ToP2);
        p1ToP2.setOnClickListener(view -> doAction(view.getId()));

        Button p2ToP1 = inflate.findViewById(R.id.p2ToP1);
        p2ToP1.setOnClickListener(view -> doAction(view.getId()));

        return inflate;
    }

    private void doAction(int id) {
        switch (id) {
            case R.id.p1ToP2:
                lifeP1--;
                lifeP2++;
                break;
            case R.id.p2ToP1:
                lifeP1++;
                lifeP2--;
                break;
            case R.id.pf1lifeless:
                lifeP1--;
                break;
            case R.id.pf1lifemore:
                lifeP1++;
                break;
            case R.id.bt1PMore:
                poisonP1++;
                break;
            case R.id.bt2Less:
                poisonP1--;
                break;
            case R.id.p2lifemore:
                lifeP2++;
                break;
            case R.id.pf2lifeless:
                lifeP2--;
                break;
            case R.id.bt3More:
                poisonP2++;
                break;
            case R.id.bt4Less:
                poisonP2--;
        }
        refreshText();
    }

    private void refreshText() {
        textView.setText(String.format("%d/%d",lifeP1, poisonP1));
        textView1.setText(String.format("%d/%d",lifeP2, poisonP2));
    }

    private void loadData(Bundle bundle) {
        this.poisonP2 = bundle.getInt("p2poison");
        this.poisonP1 = bundle.getInt("p1poison");
        this.lifeP1 = bundle.getInt("p1life");
        this.lifeP2 = bundle.getInt("p2life");
    }

    private void reset() {
        poisonP1 = 0;
        poisonP2 = 0;
        lifeP1 = 0;
        lifeP2 = 0;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt("p1life", lifeP1);
        outState.putInt("p2life", lifeP2);
        outState.putInt("p1poison", poisonP1);
        outState.putInt("p2poison", poisonP2);
    }
}
